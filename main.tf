# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_tag" "env" {
  name = var.environment
}

resource "digitalocean_tag" "role" {
  name = "website"
}

resource "digitalocean_tag" "project" {
  name = var.project
}

resource "digitalocean_ssh_key" "this" {
  name       = "Terraform"
  public_key = file(var.ssh_public_key_path)
}

resource "digitalocean_volume" "this" {
  region = var.region

  name        = format("site-data-%s", var.project)
  description = "Volume to store site data"

  size                     = var.volume_size
  initial_filesystem_type  = "ext4"
  initial_filesystem_label = "data"

  tags = [digitalocean_tag.env.name, digitalocean_tag.role.name, digitalocean_tag.project.name]
}

resource "digitalocean_droplet" "this" {
  name   = format("web-%s", var.project)
  region = var.region

  size               = "s-1vcpu-1gb"
  image              = "debian-10-x64"
  backups            = false
  monitoring         = true
  ipv6               = true
  private_networking = false

  ssh_keys = [digitalocean_ssh_key.this.fingerprint]
  tags     = [digitalocean_tag.env.name, digitalocean_tag.role.name, digitalocean_tag.project.name]

  # Setup
  provisioner "file" {
    connection {
      host        = digitalocean_droplet.this.ipv4_address
      user        = "root"
      type        = "ssh"
      private_key = file(var.ssh_private_key_path)
      timeout     = "2m"
    }

    source      = "${path.module}/setup-server.sh"
    destination = "/tmp/setup-server.sh"
  }
  provisioner "remote-exec" {
    connection {
      host        = digitalocean_droplet.this.ipv4_address
      user        = "root"
      type        = "ssh"
      private_key = file(var.ssh_private_key_path)
      timeout     = "5m"
    }

    inline = [
      "chmod +x /tmp/setup-server.sh",
      "/tmp/setup-server.sh",
    ]
  }
}

resource "digitalocean_volume_attachment" "this" {
  droplet_id = digitalocean_droplet.this.id
  volume_id  = digitalocean_volume.this.id

  # Mount Volume
  provisioner "file" {
    connection {
      host        = digitalocean_droplet.this.ipv4_address
      user        = "root"
      type        = "ssh"
      private_key = file(var.ssh_private_key_path)
      timeout     = "2m"
    }

    source      = "${path.module}/mount-volume.sh"
    destination = "/tmp/mount-volume.sh"
  }
  provisioner "remote-exec" {
    connection {
      host        = digitalocean_droplet.this.ipv4_address
      user        = "root"
      type        = "ssh"
      private_key = file(var.ssh_private_key_path)
      timeout     = "5m"
    }

    inline = [
      "chmod +x /tmp/mount-volume.sh",
      "/tmp/mount-volume.sh /dev/disk/by-id/scsi-0DO_Volume_site-data-${var.project}",
    ]
  }
}

