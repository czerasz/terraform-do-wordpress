output "ipv4_address" {
  description = "Public IPv4 addresses assigned to the Droplet"
  value       = digitalocean_droplet.this.ipv4_address
}

output "ipv4_address_private" {
  description = "Private IPv4 addresses assigned to the Droplet, if applicable"
  value       = digitalocean_droplet.this.ipv4_address_private
}

output "ipv6_address" {
  description = "Public IPv6 addresses assigned to the Droplets, if applicable"
  value       = digitalocean_droplet.this.ipv6_address
}

