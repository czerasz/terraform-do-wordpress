variable "do_token" {
  description = "DigitalOcean token"
  type        = string
}

variable "project" {
  description = "Project name"
  type        = string
}

variable "environment" {
  description = "Environment name"
  type        = string
}

variable "region" {
  description = "Region"
  type        = string
}

variable "ssh_private_key_path" {
  description = "SSH private key path"
  type        = string
}

variable "ssh_public_key_path" {
  description = "SSH public key path"
  type        = string
}

variable "volume_size" {
  description = "Volume size"
  type        = number
  default     = 1
}
