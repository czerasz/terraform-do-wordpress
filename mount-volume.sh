#!/bin/bash

set -eu

repeat_if_fail () {
  command=${@}
  echo "Execute command: ${command}"

  n=0; successful=0
  until [ $n -ge 10 ]; do
    if ${command}; then
      successful=1
      break # Continue if successful
    else
      n=$[$n+1]
      echo "Update command not successful: ${n}"
      echo 'Wait 10s for the system to come up'
      sleep 10
    fi
  done

  if [ ${successful} -eq 0 ]; then
    echo 'Command failed'
    exit 1
  else
    echo 'Command was successful'
  fi
}

volume_path=${1:-undefined}

if [ ${volume_path} == 'undefined' ]; then
  echo 'Script parameter not defined'
  exit 1
fi

# Create a mount point for your volume:
sudo mkdir -p /mnt/site_data

# Mount your volume at the newly-created mount point:
repeat_if_fail sudo mount -o discard,defaults,noatime "${volume_path}" /mnt/site_data

# Change fstab so the volume will be mounted after a reboot
sudo echo "${volume_path} /mnt/site_data ext4 defaults,nofail,discard 0 0" | sudo tee -a /etc/fstab

# Create required directories
mkdir -p /mnt/site_data/{db,certbot,wordpress/wp-content}
