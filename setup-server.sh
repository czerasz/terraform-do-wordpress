#!/bin/bash

set -eu

repeat_if_fail () {
  command=${@}
  echo "Execute command: ${command}"

  n=0; successful=0
  until [ $n -ge 5 ]; do
    if ${command}; then
      successful=1
      break # Continue if successful
    else
      n=$[$n+1]
      echo "Update command not successful: ${n}"
      echo 'Wait 10s for the system to come up'
      sleep 10
    fi
  done

  if [ ${successful} -eq 0 ]; then
    echo 'Command failed'
    exit 1
  else
    echo 'Command was successful'
  fi
}

# Install Docker
# Resource: https://docs.docker.com/install/linux/docker-ce/debian/
# repeat_if_fail curl -s dekos | grep -v 'Wait'
repeat_if_fail sudo apt-get update

repeat_if_fail sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"

sudo apt-get update

sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# Extend DO monitoring
# Resoure: https://www.digitalocean.com/docs/monitoring/how-to/install-agent/
curl -sSL https://repos.insights.digitalocean.com/install.sh | sudo bash

# Required software
sudo apt-get install -y rsync \
  vim \
  jq \
  gzip

sudo curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
